# FSW5-ChallengeCh07-ReactJS

## Anggota Kelompok  
1. Muhamad Arif Hidayat 
2. Hafid Zaeny 
3. Hana Ghaliyah Azhar
4. Raihan Iqbal 

## Deskripsi
Pada challenge chapter 07 ini, kami membuat website Binar Car Rental dengan menerapkan ReactJS pada frontend-nya, menggunakan OAuth untuk melakukan login dan register akun user, menerapkan konsep redux untuk mengelola data mobil. <br/>
Data mobil disimpan di backend meliputi filter data mobil berdasarkan tipe driver, tanggal, waktu, dan jumlah penumpang.

## Running Frontend
```sh
cd frontend
npm start
```

## Running Server Backend
```sh
cd server
npm run start
```

## Cara Menjalankan Program
* buka project pada Text Editor
* jalankan npm install pada direktori frontend dan server
* lalu jalankan npm run start pada direktori server
* dan npm start pada direktori frontend
* kemudian buka localhost:3000

## Endpoint 
http://localhost:3000/      : Landing page <br />
http://localhost:3000/signin : Login OAuth Google <br />
http://localhost:3000/cars  : Cars Get All Data and Filtering
