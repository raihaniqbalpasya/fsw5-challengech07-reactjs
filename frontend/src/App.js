import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { gapi } from "gapi-script";
import React, { useEffect, useState } from "react";

// redux
import { Provider } from "react-redux";
import store from "./redux/store";

import LandingPage from './pages/LandingPage';
import Cars from './pages/Cars';
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import Protected from './pages/Protected'

import './assets/css/style.css'

function App() {
  const [loginData, setLoginData] = useState(
    localStorage.getItem("loginData")
      ? JSON.parse(localStorage.getItem("loginData"))
      : null
  );

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID,
      });
    }
    gapi.load("client:auth2", start);
  });

  const handleLogin = async (googleData) => {
    const res = await fetch("http://localhost:8000/api/google-login", {
      method: "POST",
      body: JSON.stringify({
        token: googleData.tokenId,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    setLoginData(data);
    localStorage.setItem("loginData", JSON.stringify(data));
  };

  const handleFailure = (result) => {
    alert("Login Gagal");
  };

  const handleLogout = () => {
    localStorage.removeItem("loginData");
    setLoginData(null);
  };
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Routes>
            <Route exact path='/' element={
              <LandingPage
                loginData={loginData}
                handleLogout={handleLogout}
              />
            } />
            <Route path="/cars" element={
              <Protected
                loginData={loginData}
                children={<Cars
                  loginData={loginData}
                  handleLogout={handleLogout}
                />}
              />
            } />
            <Route path="/signin" element={
              <SignIn
                handleLogin={handleLogin}
                handleLogout={handleLogout}
                handleFailure={handleFailure}
                loginData={loginData}
              />
            } />
            <Route path="/signup" element={<SignUp />} />
          </Routes>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
