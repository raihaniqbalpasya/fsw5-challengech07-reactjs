import React from 'react'

//import component
import Navbar from '../parts/Navbar'
import Header from '../parts/Header'
import FilterSearch from '../parts/FilterSearch'
import Footer from '../parts/Footer'


export default function Cars(props) {
    const { loginData, handleLogout } = props
    return <>
        <Navbar loginData={loginData} handleLogout={handleLogout} />
        <Header />
        <div className='container'>
            <FilterSearch />
        </div>
        <Footer />
    </>
}
