import React from "react";
import { GoogleLogin, GoogleLogout } from "react-google-login";
import LandingPage from '../assets/images/landingpage.png'

export default function SignIn(props) {
    const { handleLogin, handleLogout, handleFailure, loginData } = props

    return <>
        {loginData ? (
            <section className='sign pt-0'>
                <div className="row mx-0">
                    <div className="col-7 align-self-center">
                        <div className="form-sign">
                            <a className='brand' href="/"></a>
                            <h2>Anda Sudah Login!</h2>

                            <GoogleLogout
                                clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                                buttonText="Logout"
                                onLogoutSuccess={handleLogout}
                                className="google-sign"
                            ></GoogleLogout>
                            <p className='to-signin'>Pergi Ke Home! &nbsp;
                                <a href="/">BINAR CAR RENTAL</a>
                            </p>

                        </div>
                    </div>
                    <div className="col-5">
                        <div className='right-header'>
                            <h1 className="title">
                                Binar Car Rental
                            </h1>
                            <img className='img-fluid' src={LandingPage} alt="" />
                        </div>
                    </div>
                </div>
            </section>
        ) : (
            <section className='sign pt-0'>
                <div className="row mx-0">
                    <div className="col-7 align-self-center">
                        <div className="form-sign">
                            <a className='brand' href="/"></a>
                            <h2>Welcome Back!</h2>
                            <form>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                                    <input type="email" className="form-control" id="inputEmail" placeholder='Contoh: johndee@gmail.com' />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="inputPassword" className="form-label">Password</label>
                                    <input type="password" className="form-control" id="inputPassword" placeholder='6+ karakter' />
                                </div>
                                <button type="submit" className="btn btn-sign">SignIn</button>

                                <GoogleLogin
                                    clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                                    buttonText="Login with Google"
                                    onSuccess={handleLogin}
                                    onFailure={handleFailure}
                                    cookiePolicy={"single_host_origin"}
                                    isSignedIn={true}
                                    className="google-sign"
                                ></GoogleLogin>

                                <p className='to-signin'>Don’t have an account? &nbsp;
                                    <a href="/signup">Sign Up for free</a>
                                </p>
                            </form>
                        </div>
                    </div>
                    <div className="col-5">
                        <div className='right-header'>
                            <h1 className="title">
                                Binar Car Rental
                            </h1>
                            <img className='img-fluid' src={LandingPage} alt="" />
                        </div>
                    </div>
                </div>
            </section>
        )}
    </>


}
