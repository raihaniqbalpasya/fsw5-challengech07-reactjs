import { Navigate } from "react-router-dom";

function Protected(props) {
    const { children, loginData } = props
    return <>
        {
            loginData ? (
                children
            ) : (
                <Navigate to="/signin" />
            )
        }
    </>
}

export default Protected;
